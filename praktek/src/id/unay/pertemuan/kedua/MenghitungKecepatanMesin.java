package id.unay.pertemuan.kedua;

import java.util.Scanner;

/**
 *
 * @author Unay
 */
public class MenghitungKecepatanMesin {
    public static void main(String[] args) {
        double pjgtanah, lbrtanah, pjgrumah, lbrrumah, kecepatan, hasil;
        
        Scanner in = new Scanner (System.in);
        pjgtanah = in.nextDouble();
        lbrtanah = in.nextDouble();
        pjgrumah = in.nextDouble();
        lbrrumah = in.nextDouble();
        kecepatan = 2.5;
        hasil = ((lbrtanah * pjgtanah) - (pjgrumah * lbrrumah)) / kecepatan;
        System.out.println("hasilnya adalah " + hasil);
    }
}
