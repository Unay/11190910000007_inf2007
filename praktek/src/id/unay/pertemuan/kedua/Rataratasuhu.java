package id.unay.pertemuan.kedua;

import java.util.Scanner;

/**
 *
 * @author Unay
 */
public class Rataratasuhu {
    public static void main(String[] args) {
        int x, y, z;
        
        Scanner in = new Scanner(System.in);
        x = in.nextInt(); //suhu minimal//
        y = in.nextInt(); //suhu maximal //
        z = (x + y)/2;
        
        System.out.println("suhu minimal suatu hari tertentu adalah " + x);
        System.out.println("Suhu maximal suatu hari tertentu adalah " + y);
        System.out.println("Suhu rata-rata dalam suatu hari tertentu adalah " + z);
    }
}
