package id.unay.pertemuan.ketujuh;

import java.util.Scanner;

public class AplikasiSegitigaParameter {

    public static void main(String[] args) {
        SegitigaParameter segitiga2 = new SegitigaParameter();
        
        Scanner in = new Scanner(System.in);
        double alas, tinggi;
        
        System.out.print("Masukkan alas: ");
        alas = in.nextDouble();
        
        System.out.print("Masukkan tinggi: ");
        tinggi = in.nextDouble();
        
        SegitigaParameter segitiga = new SegitigaParameter(alas, tinggi);
        segitiga.getInfo();
        System.out.println("Luas : " + segitiga.getLuas());
    }
}
