package id.unay.pertemuan.ketujuh;

public class SegitigaParameter {
    private double alas;
    private double tinggi;
    
    public SegitigaParameter(){
        System.out.println("Konstruktor Segitiga");
    }
    public SegitigaParameter(double alas, double tinggi){
        this.alas = alas;
        this.tinggi = tinggi;
    }
    public double getLuas(){
        return (alas * tinggi) / 2;
    }
    public void getInfo(){
            System.out.println("Kelas Segitiga");
    }
}