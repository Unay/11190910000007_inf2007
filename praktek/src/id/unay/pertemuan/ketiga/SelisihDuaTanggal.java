package id.unay.pertemuan.ketiga;

import java.util.Scanner;

public class SelisihDuaTanggal {
    public static void main(String[] args) {
        int Tanggal1, Bulan1, Tahun1, Tanggal2, Bulan2, Tahun2, Tanggal3, Bulan3, Tahun3, Selisih;
        Scanner in = new Scanner(System.in);
        System.out.print("Tanggal 1 : ");
        Tanggal1 = in.nextInt();
        System.out.print("Bulan 1 : ");
        Bulan1 = in.nextInt();
        System.out.print("Tahun 1 : ");
        Tahun1 = in.nextInt();
        System.out.print("Tanggal 2 : ");
        Tanggal2 = in.nextInt();
        System.out.print("Bulan 2 : ");
        Bulan2 = in.nextInt();
        System.out.print("Tahun 2 : ");
        Tahun2 = in.nextInt();
        Selisih = (Tahun2 - Tahun1)*365 + (Bulan2 - Bulan1)*30 + (Tanggal2 - Tanggal1);
        Tahun3 = Selisih / 365;
        Bulan3 = (Selisih % 365) / 30;
        Tanggal3 = (Selisih % 365) % 30;
        System.out.println("Selisih ");
        System.out.print(Tahun3 + " Tahun " + Bulan3 + " Bulan " + Tanggal3 + " Hari ");
    }
}
