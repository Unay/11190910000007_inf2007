package id.unay.pertemuan.ketiga;

import java.util.Scanner;

public class JarakSemut {
    public static void main(String[] args) {
        int Jarak, Kilometer, Meter, Sentimeter;
        Scanner in = new Scanner(System.in);
        System.out.print("Jarak (cm) : ");
        Jarak = in.nextInt();
        
        Kilometer = Jarak / 100000;
        Meter = (Jarak % 100000) / 100;
        Sentimeter = (Jarak % 100000) % 100;
        
        System.out.println("Jarak = " + Kilometer + " km, " + Meter + " m, " + Sentimeter + " cm. ");
    }
}