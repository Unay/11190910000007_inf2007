package id.unay.pertemuan.ketiga;

import java.util.Scanner;

public class UangPecahan {
    public static void main(String[] args) {
        int Uang, p1000, p500, p100, p50, p25;
        Scanner in = new Scanner(System.in);
        System.out.print("Jumlah Uang : ");
        Uang = in.nextInt();
        
        p1000 = Uang / 1000;
        p500 = (Uang % 1000) / 500;
        p100 = (Uang % 500) / 100;
        p50 = (Uang % 100) / 50;
        p25 = (Uang % 50) / 25;
        
        System.out.println("Uang Pecahan");
        System.out.println("Rp 1000 = " + p1000 + " buah");
        System.out.println("Rp 500 = " + p500 + " buah");
        System.out.println("Rp 100 = " + p100 + " buah");
        System.out.println("Rp 50 = " + p50 + " buah");
        System.out.println("Rp 25 = " + p25 + " buah");
    }
} 
