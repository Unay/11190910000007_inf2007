package id.unay.pertemuan.kelima;

import java.util.Scanner;

public class MenghitungRataRata {
    public static void main(String[] args) {
        int i, x, jumlah;
        float rerata;

        jumlah = 0;
        i = 0;
        Scanner in = new Scanner(System.in);
        System.out.print("x = ");
        x = in.nextInt();

        while (x != -1) {
            i = i + 1;
            jumlah = jumlah + x;
            System.out.print("x = ");
            x = in.nextInt();
        }
        if (i != 0) {
            rerata = (float) jumlah / i;
            System.out.println("rata-rata = " + rerata);
        } else {
            System.out.println("tidak ada nilai ujian yang dimasukkan");
        }
    }
}
