package id.unay.pertemuan.keduabelas;

/**
 *
 * @author Unay
 */
public class PenyimpananUangTest {
     public static void main(String[] args) {
        PenyimpananUang tabungan = new PenyimpananUang(5000, 8.5 / 100);
        System.out.println("uang yang ditabung: 5000");
        System.out.println("tingkat bunga sekarang: 8.5%");
        System.out.println("Total uang anda sekarang: " + tabungan.cekUang());
    }
}
