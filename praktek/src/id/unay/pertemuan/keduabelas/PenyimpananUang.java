package id.unay.pertemuan.keduabelas;

/**
 *
 * @author Unay
 */
public class PenyimpananUang extends Tabungan {
    private double tingkatBunga;

    public PenyimpananUang(int saldo, double tingkatBunga) {
        super(saldo);
        this.tingkatBunga = tingkatBunga;
    }
    
    public double cekUang() {
        return tingkatBunga = saldo + (saldo * tingkatBunga);
    }
}
