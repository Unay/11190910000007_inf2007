package id.unay.pertemuan.keduabelas;

/**
 *
 * @author Unay
 */
public class TabunganTest {
     public static void main(String[] args) {
        Tabungan t = new Tabungan (5000);
        System.out.println("Saldo Awal: " + t.saldo);
        t.ambilUang(2300);
        System.out.println("Jumlah Uang yang diambil: 2300");
        System.out.println("Saldo Sekarang: " + t.saldo);
    }
}
