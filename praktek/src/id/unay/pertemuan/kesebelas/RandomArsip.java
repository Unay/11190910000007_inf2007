package id.unay.pertemuan.kesebelas;

import java.io.IOException;
import java.io.RandomAccessFile;

/**
 *
 * @author Unay
 */
public class RandomArsip {
    public void setTulis (String file,int position, String record) {
        try {
            RandomAccessFile raf = new RandomAccessFile(file, "rw");
            raf.seek(position);
            raf.writeUTF(record);
            raf.close();
        } catch (IOException e) {
            System.err.println("Eror : " + e.getMessage());
        }
    }
    
    public String getBaca(String file, int position) {
        String record = "";
        try {
            RandomAccessFile raf = new RandomAccessFile(file, "r");
            raf.seek(position);
            record = raf.readUTF();
            raf .close();
        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());
        }
        return record;
    }
    
    public static void main(String[] args) {
        String berkas = "D:\\DDP\\random.txt";
        String data = "NIM : 123 | Nama : Budi ";
        
        RandomArsip ra = new RandomArsip();
        ra.setTulis(berkas, 1, data);
        System.out.println("Tulis Berhasil");
        
        String Output = ra.getBaca(berkas, 1);
        System.out.println("Baca berhasil : " + Output);
    }
}
