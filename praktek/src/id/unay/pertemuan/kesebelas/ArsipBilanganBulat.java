package id.unay.pertemuan.kesebelas;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.Scanner;

/**
 *
 * @author Unay
 */
public class ArsipBilanganBulat {
    public static void main(String[] args) {
        File file = new File ("D:\\DDP\\intArsip.txt");
        int n, i;
        Scanner in = new Scanner (System.in);
        System.out.print("Masukan Nilai : ");
        
        try {
            PrintWriter output = new PrintWriter(file);
            n = in.nextInt();
            for (i = 0; i < n; i++) {
                output.println(i);
            }
            output.close();
        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());
        }
    }
}