package id.unay.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author Unay
 */
public class BubbleSort {
    int [] getBubbleSort (int n, int L[]) { 
        int i, k, temp;
        
        for (i = 0; i < n - 1; i++) {
            for (k = n - 1; k > i; k--){
                System.out.println("i : " + i +", k : " + (k - 1) + " --> " + L[k - 1]);
                if (L[k] < L[k - 1]){
                    temp = L[k];
                    L[k] = L[k-1];
                    L[k-1] = temp;
                }
            }   
        }
        return L;
    }

    public static void main(String[] args) {
        int L[] = {25, 27, 10, 8, 76, 21};
        int i,n = L.length;
        BubbleSort fungsi = new BubbleSort();
    System.out.println(Arrays.toString(L));
        fungsi.getBubbleSort(n, L);
        System.out.println(Arrays.toString(L));
    }
}