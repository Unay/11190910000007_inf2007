package id.unay.pertemuan.keempat;

import java.util.Scanner;

public class KonversiAngkaKeTeks {
    public static void main(String[] args) {
        int angka;
        Scanner in = new Scanner(System.in);
        System.out.print("masukkan angka : ");
        angka = in.nextInt();

        switch (angka) {
            case 1:
                System.out.println("Satu");
                break;
            case 2:
                System.out.println("Dua");
                break;
            case 3:
                System.out.println("Tiga");
                break;
            case 4:
                System.out.println("Empat");
                break;
            default:
                System.out.println("angka yang dimasukkan salah");
        }
    }
}
