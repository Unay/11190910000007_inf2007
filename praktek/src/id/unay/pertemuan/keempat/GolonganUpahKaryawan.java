package id.unay.pertemuan.keempat;

import java.util.Scanner;

public class GolonganUpahKaryawan {
     public static void main(String[] args) {
        int JJK, jamLembur, jamNormal, upahLembur, upahPerJam = 0;
        char gol;
        String nama;
        float upahTotal;

        jamNormal = 48;
        upahLembur = 3000;

        Scanner in = new Scanner(System.in);
        System.out.print("Nama = ");
        nama = in.nextLine();
        System.out.print("gol = ");
        gol = in.next().charAt(0);
        System.out.print("Jam kerja = ");
        JJK = in.nextInt();

        if (gol == 'A') {
            upahPerJam = 4000;
        }else {
            if (gol == 'B') {
                upahPerJam = 5000;
            } else {
                if (gol == 'C') {
                    upahPerJam = 6000;
                } else {
                    if (gol == 'D') {
                        upahPerJam = 7500;
                    }
                }
            }
            if (JJK <= jamNormal) {
                upahTotal = JJK * upahPerJam;
            } else {
                jamLembur = JJK - jamNormal;
                upahTotal = jamNormal * upahPerJam + jamLembur * upahLembur;

                System.out.println("nama : " + nama + "\nupah : Rp. " + upahTotal);
            }
        }
    }
}