package id.unay.pertemuan.keempat;

import java.util.Scanner;

public class BilanganTerbesar {
    public static void main(String[] args) {
        int a, b, c, maks;
        Scanner in = new Scanner(System.in);
        System.out.print("a = ");
        a = in.nextInt();
        System.out.print("b = ");
        b = in.nextInt();
        System.out.print("c = ");
        c = in.nextInt();
        if ((a > b) && (a > c)) {
            System.out.println("Bilangan terbesar = " + a);
        } else {
            if ((b > a) && (b > c)) {
                System.out.println("Bilangan terbesar = " + b);
            } else {
                System.out.println("Bilangan terbesar = " + c);
            }
        }
    }
}
