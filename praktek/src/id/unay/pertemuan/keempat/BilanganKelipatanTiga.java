package id.unay.pertemuan.keempat;

import java.util.Scanner;

public class BilanganKelipatanTiga {
    public static void main(String[] args) {
        int Bilangan;
        Scanner in = new Scanner(System.in);
        System.out.print("bilangan = ");
        Bilangan = in.nextInt();

        if (Bilangan % 3 == 0) {
            System.out.println("Bilangan kelipatan tiga ");
        } else {
            System.out.println("Bukan kelipatan tiga ");
        }
    }
}