package id.unay.pertemuan.keempat;

import java.util.Scanner;

public class DiskonHarga {
    public static void main(String[] args) {
        int totalbelanja;
        float diskon = 0, total;
        Scanner in = new Scanner(System.in);
        System.out.print("Total belanja = ");
        totalbelanja = in.nextInt();

        if (totalbelanja > 120000) {
            diskon = totalbelanja * 7 / 100;
            total = totalbelanja - diskon;
        } else {
            total = totalbelanja;
        }
        System.out.println("Diskon = Rp. " + diskon);
        System.out.println("Harga belanja = Rp. " + total);
    }
}
