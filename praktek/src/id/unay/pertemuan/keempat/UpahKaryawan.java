package id.unay.pertemuan.keempat;

import java.util.Scanner;

public class UpahKaryawan {
    public static void main(String[] args) {
        int JJK, jamNormal, upahPerJam, upahLembur;
        String nama;
        float lembur, upah;

        jamNormal = 48;
        upahPerJam = 2000;
        upahLembur = 3000;

        Scanner in = new Scanner(System.in);
        System.out.print("Nama = ");
        nama = in.nextLine();
        System.out.print("Jam kerja = ");
        JJK = in.nextInt();

        if (JJK <= jamNormal) {
            upah = JJK * upahPerJam;
        } else {
            lembur = JJK - jamNormal;
            upah = (jamNormal * upahPerJam) + (lembur * upahLembur);
        }
        System.out.println("Nama : " + nama);
        System.out.println("upah = " + upah);
    }
}
