package id.unay.pertemuan.keempat;

import java.util.Scanner;

public class BilanganGenap {
    public static void main(String[] args) {
        Scanner in = new Scanner (System.in);
        System.out.print("Bilangan Bulat : ");
        
        int bilangan= in.nextInt();
        if (bilangan % 2 == 1) {
            System.out.println("Bilangan Ganjil ");
        }
        else
            System.out.println("Bilangan Genap ");
    }
}
