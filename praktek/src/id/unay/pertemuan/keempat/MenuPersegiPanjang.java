package id.unay.pertemuan.keempat;

import static java.lang.Math.sqrt;
import java.util.Scanner;

public class MenuPersegiPanjang {
    public static void main(String[] args) {
        int noMenu;
        float panjang, lebar, luas, keliling, diagonal;

        System.out.println("Menu Empat Persegi Panjang");
        System.out.println("\t1. Hitung Luas");
        System.out.println("\t2. Hitung Keliling");
        System.out.println("\t3. Hitung Panjang Diagonal");
        System.out.println("\t4. Keluar Program");
        System.out.print("Masukkan pilihan (1/2/3/4) ? ");
        Scanner in = new Scanner(System.in);
        noMenu = in.nextInt();

        switch (noMenu) {
            case 1:
                System.out.print("panjang = ");
                panjang = in.nextInt();
                System.out.print("lebar = ");
                lebar = in.nextInt();
                luas = panjang * lebar;
                System.out.println("Luas = " + luas);
                break;
            case 2:
                System.out.print("panjang = ");
                panjang = in.nextInt();
                System.out.print("lebar = ");
                lebar = in.nextInt();
                keliling = 2 * panjang + 2 * lebar;
                System.out.println("Keliling = " + keliling);
                break;
            case 3:
                System.out.print("panjang = ");
                panjang = in.nextInt();
                System.out.print("lebar = ");
                lebar = in.nextInt();
                diagonal = (float) sqrt(panjang * panjang + lebar * lebar);
                System.out.println("Diagonal = " + diagonal);
                break;
            case 4:
                System.out.print("keluar program... sampai jumpa");
                break;
        }
    }
}
