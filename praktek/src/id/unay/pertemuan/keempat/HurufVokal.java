package id.unay.pertemuan.keempat;

import java.util.Scanner;

public class HurufVokal {
    public static void main(String[] args) {
        char huruf;
        Scanner in = new Scanner(System.in);
        System.out.print("Huruf = ");
        
        huruf= in.next().charAt(0);
        if (huruf == 'a' ||huruf == 'i' ||huruf == 'u' ||huruf == 'e' ||huruf == 'o'){
            System.out.println("Huruf Vokal ");
        }
        else
            System.out.print("Huruf Konsonan");
        }
    }
