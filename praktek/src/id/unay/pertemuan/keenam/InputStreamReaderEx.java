package id.unay.pertemuan.keenam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class InputStreamReaderEx {
    public static void main(String[] args) {
        int bilangan;
        BufferedReader in = new BufferedReader(new InputStreamReader (System.in));
        
        System.out.print("Masukkan Bilangan: ");
        try {
            bilangan = Integer.parseInt(in.readLine());
            
            System.out.println("Bilangan: " + bilangan);
        } catch (IOException ex) {
            System.out.print("error: " + ex.toString());
        }
    }
}
