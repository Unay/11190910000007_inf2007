package id.unay.Peminjaman.controller;

import com.google.gson.Gson;
import id.unay.Peminjaman.model.Peminjaman;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
/**
 *
 * @author Unay
 */
public class PeminjamanController {
    private static final String FILE = "C:\\Users\\ASUS\\.ssh\\11190910000007_Inf2007\\Project\\PeminajamanRuangan\\lib\\pinjam.json";
    private Peminjaman pinjam;
    private final Scanner in;
    private String nama;
    private String nim;
    private String Kelas;
    private String Dosen;
    private String Ruangan;
    private final LocalDateTime waktupinjam;
    private LocalDateTime waktukembali;
    private final DateTimeFormatter dateTimeFormat;
    private int pilihan;

    public PeminjamanController () {
        in = new Scanner(System.in);
        waktupinjam = LocalDateTime.now();
        waktukembali = LocalDateTime.now();
        dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    }
    
    public void setPeminjamanRuangan() {
        System.out.print("Masukkan Nama : ");
        nama = in.next();
        System.out.print("Masukkan NIM : ");
        nim = in.next();
        System.out.print("Masukan Kelas : ");
        Kelas = in.next();
        System.out.print("Dosen/guru Penanggung Jawab : ");
        Dosen = in.next();
        System.out.print("Ruangan Yang Di Pinjam : ");
        Ruangan = in.next();

        String formatWaktuMasuk = waktupinjam.format(dateTimeFormat);
        System.out.println("Waktu Peminjaman : " + formatWaktuMasuk);

        pinjam = new Peminjaman();
        pinjam.setNama(nama.toUpperCase());
        pinjam.setNim(nim);
        pinjam.setKelas(Kelas);
        pinjam.setDosen(Dosen);
        pinjam.setRuangan(Ruangan);
        pinjam.setWaktupinjam(waktupinjam);

        setWritePeminjaman(FILE, pinjam);

        System.out.println("Apakah mau Input kembali?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            setPeminjamanRuangan();
        }
    }
    
    /**
     * Method untuk menyimpan data pinjam ke file berbentuk json
     *
     * @param file, tipedata <code>String</code> merujuk ke file yang akan
     * disimpan.
     * @param pinjam, model <code>Peminjaman</code> yang akan disimpan.
     */
    public void setWritePeminjaman(String file, Peminjaman pinjam) {
        Gson gson = new Gson();

        List<Peminjaman> pinjams = getReadPeminjaman(file);
        pinjams.remove(pinjam);
        pinjams.add(pinjam);

        String json = gson.toJson(pinjams);
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(json);
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(PeminjamanController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void setPengembalianRuangan() {
        System.out.print("Masukkan Nama : ");
        nama = in.next();

        Peminjaman p = getSearch(nama);
        if (p != null) {
            LocalDateTime tempWaktu = LocalDateTime.from(p.getWaktupinjam());
            waktukembali = LocalDateTime.now();
            long jam = tempWaktu.until(waktukembali, ChronoUnit.HOURS);
            p.setWaktukembali(waktukembali);
            p.setKeluar(true);
          
            System.out.println("Nama : " + p.getNama());
            System.out.println("NIM : " + p.getNim());
            System.out.println("Kelas : " + p.getKelas());
            System.out.println("Dosen Penanggung Jawab : " + p.getDosen());
            System.out.println("Ruangan : " + p.getRuangan());
            System.out.println("Waktu Peminjaman : " + p.getWaktupinjam().format(dateTimeFormat));
            System.out.println("Waktu Pengembalian : " + p.getWaktukembali().format(dateTimeFormat));
 
            System.out.println("Menuju Data Peminjaman?");
            System.out.print("1) Ya, 2) Tidak, 3) Keluar : ");
            pilihan = in.nextInt();
            switch (pilihan) {
                case 1:
                    setWritePeminjaman(FILE, p);
                    break;
                case 2:
                    setPengembalianRuangan();
                    break;
                default:
                    Menu m = new Menu();
                    m.getMenuAwal();
                    break;
            }

            System.out.println("Apakah mau memproses kembali?");
            System.out.print("1) Ya, 2) Tidak : ");
            pilihan = in.nextInt();
            if (pilihan == 2) {
                Menu m = new Menu();
                m.getMenuAwal();
            } else {
                setPengembalianRuangan();
            } 
        } else {
            System.out.println("Data tidak ditemukan");
            setPeminjamanRuangan();
        }
    }
    
    /**
     * Method untuk mencari nomor polisi dari data file json
     *
     * @param nama, tipedata <code>String</code> merujuk ke data yang akan
     * dicari.
     * @return <code>Peminjaman</code>
     */
    public Peminjaman getSearch(String nama) {
        List<Peminjaman> pinjams = getReadPeminjaman(FILE);// Arrays.asList(pinjam);

        Peminjaman p = pinjams.stream()
                .filter(pp -> nama.equalsIgnoreCase(pp.getNama()))
                .findAny()
                .orElse(null);

        return p;
    }
    
    /**
     * Method untuk mencari nama dari data file json
     *
     * @param file, tipedata <code>String</code> merujuk ke file penyimpanan
     * data.
     * @return <code>List<Peminjaman></code>
     */
     public List<Peminjaman> getReadPeminjaman(String file) {
        List<Peminjaman> pinjams = new ArrayList<>();

        Gson gson = new Gson();
        String line = null;
        try ( Reader reader = new FileReader(file)) {
            BufferedReader br = new BufferedReader(reader);
            while ((line = br.readLine()) != null) {
                Peminjaman[] ps = gson.fromJson(line, Peminjaman[].class);
                pinjams.addAll(Arrays.asList(ps));
            }
            br.close();
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PeminjamanController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PeminjamanController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return pinjams;
    }
     /**
     * Method untuk menampilkan data pinjam yang sudah keluar
     */
    public void getDataPeminjaman() {
        List<Peminjaman> pinjams = getReadPeminjaman(FILE);
        Predicate<Peminjaman> isKeluar = e -> e.isKeluar() == true;
        Predicate<Peminjaman> isDate = e -> e.getWaktukembali().toLocalDate().equals(LocalDate.now());

        List<Peminjaman> pResults = pinjams.stream().filter(isKeluar.and(isDate)).collect(Collectors.toList());
       
        System.out.println("Nama \t      NIM      \tKelas \tDosen Penanggung Jawab \tRuangan \tWaktu Peminjaman\tWaktu Pengembalian");
        System.out.println("---- \t-------------- \t-----\t----------------------\t------- \t----------------\t------------------");
        pResults.forEach((p) -> {
            System.out.println(p.getNama()+ "\t" + p.getNim() + "\t" + p.getKelas() + "\t\t" + p.getDosen() + "\t\t" + p.getRuangan() + "\t\t" + p.getWaktupinjam().format(dateTimeFormat) + "\t" + p.getWaktukembali().format(dateTimeFormat));
        });
        System.out.println("---- \t-------------- \t------\t---------------------- \t------- \t----------------\t------------------");
        System.out.println("Apakah mau mengulanginya?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            getDataPeminjaman();
        }
    }
}
