package id.unay.Peminjaman.controller;

import id.unay.Peminjaman.model.Info;
import java.util.Scanner;

/**
 *
 * @author Unay
 */
public class Menu {
    private final Scanner in = new Scanner(System.in);
    private int noMenu;

    public void getMenuAwal() {
        Info info = new Info();

        System.out.println("=================================================");
        System.out.println(info.getAplikasi());
        System.out.println(info.getVersion());
        System.out.println("-------------------------------------------------");

        System.out.println("Selamat Datang Di Menu Utama");
        System.out.println("--Silahkan Pilih Menu Anda--");
        System.out.println("1. Peminjaman Ruangan");
        System.out.println("2. Pengembalian Ruangan");
        System.out.println("3. Laporan Peminjaman");
        System.out.println("4. Keluar Aplikasi");
        System.out.println("=================================================");
        System.out.print("Pilih Menu (1/2/3/4) : ");

        do {
            while (!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("\"%s\" is not a valid number.\n", input);
                System.out.print("Pilih Menu (1/2/3/4) : ");
            }
            noMenu = in.nextInt();
        } while (noMenu < 0);
        setPilihMenu();
    }

    public void setPilihMenu() {
        PeminjamanController pc = new PeminjamanController();
        switch (noMenu) {
            case 1:
                pc.setPeminjamanRuangan();
                break;
            case 2:
                pc.setPengembalianRuangan();
                break;
            case 3:
                pc.getDataPeminjaman();
                break;
            case 4:
                System.out.println("Sampai jumpa :)");
                System.exit(0);
                break;
        }
    }
}
