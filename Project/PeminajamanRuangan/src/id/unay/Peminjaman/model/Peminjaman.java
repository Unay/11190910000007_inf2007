package id.unay.Peminjaman.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 *
 * @author Unay
 */
public class Peminjaman implements Serializable {

    private static final long serialVersionUID = -6756463875294313469L;

    private String nama;
    private String nim;
    private String Kelas;
    private String Dosen;
    private String Ruangan;
    private int jenis;
    private LocalDateTime waktupinjam;
    private LocalDateTime waktukembali;
    private boolean keluar = false;

    public Peminjaman() {

    }
    
    public Peminjaman (String nama,String NIM, String Kelas,String Dosen, String Ruangan, int jenis, LocalDateTime waktupinjam, LocalDateTime waktukembali) {
        this.nama = nama;
        this.nim = NIM;
        this.Kelas = Kelas;
        this.Dosen = Dosen;
        this.Ruangan = Ruangan;
        this.jenis = jenis;
        this.waktupinjam = waktupinjam;
        this.waktukembali = waktukembali;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }
    public String getDosen() {
        return Dosen;
    }

    public void setDosen(String Dosen) {
        this.Dosen = Dosen;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKelas() {
        return Kelas;
    }

    public void setKelas(String Kelas) {
        this.Kelas = Kelas;
    }

    public String getRuangan() {
        return Ruangan;
    }

    public void setRuangan(String Ruangan) {
        this.Ruangan = Ruangan;
    }

    public int getJenis() {
        return jenis;
    }

    public void setJenis(int jenis) {
        this.jenis = jenis;
    }

    public LocalDateTime getWaktupinjam() {
        return waktupinjam;
    }

    public void setWaktupinjam(LocalDateTime waktupinjam) {
        this.waktupinjam = waktupinjam;
    }

    public LocalDateTime getWaktukembali() {
        return waktukembali;
    }

    public void setWaktukembali(LocalDateTime waktukembali) {
        this.waktukembali = waktukembali;
    }

    public boolean isKeluar() {
        return keluar;
    }

    public void setKeluar(boolean keluar) {
        this.keluar = keluar;
    }
    @Override
    public String toString() {
        return "Peminjaman{" + "nama=" + nama + ", NIM=" + nim + ", Kelas=" + Kelas + ", Ruangan=" + Ruangan + ", Dosen=" + Dosen + ", waktuPinjam=" + waktupinjam + ", waktukembali=" + waktukembali + ", keluar=" + keluar + '}';
    }
}
//