package id.unay.Peminjaman.model;

/**
 *
 * @author Unay
 */
public class Info {
    private final String aplikasi = "Aplikasi Peminjaman Ruangan Sederhana";
    private final String version = "Versi 1.0.0";

    public String getAplikasi() {
        return aplikasi;
    }

    public String getVersion() {
        return version;
    }
}
